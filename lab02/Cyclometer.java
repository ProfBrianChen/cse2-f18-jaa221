// Jackson Aguas
// 9/6/18
// This program is a bicycle cyclometer
public class Cyclometer
  // main method used to begin a java program
{
  public static void main(String[] args)
  {
    double secsTrip1 = 480.0; // Declares the seconds variable for trip 1
    double secsTrip2 = 3220.0; // Declares the seconds variable for trip 2
    int countsTrip1 = 1561; // Declares the rotations variable for trip 1
    int countsTrip2 = 9037; // Declares the rotations variable for trip 2
    double wheelDiameter = 27.0; // Declares the wheel diameter variable
    double PI = 3.141592; // Sets Pi
    int feetPerMile = 5280; // Sets feet per mile
    int inchesPerFoot = 12; // Sets inches per foot
    int secondsPerMinute = 60; // Sets seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; // Creates empty variables for the total distances
    System.out.println(); // Creates space in the terminal for output
    System.out.println("Trip 1 took " 
               + (secsTrip1 / secondsPerMinute) + " minutes and had " 
                + countsTrip1 + " counts."); // Prints time and counts of trip 1
    System.out.println("Trip 2 took " 
               + (secsTrip2 / secondsPerMinute) + " minutes and had " 
                + countsTrip2 + " counts."); // Prints time and counts of trip 2
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // Gives distance in inches of trip 1
    distanceTrip1 = distanceTrip1/(inchesPerFoot * feetPerMile); //Converts distance from inches to miles for trip 1
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; // Gives distance in inches of trip 2
    distanceTrip2 = distanceTrip2/(inchesPerFoot * feetPerMile); //Converts distance from inches to miles for trip 2
    totalDistance = distanceTrip1 + distanceTrip2;
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // Prints distance of trip 1 in miles
    System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // Prints distance of trip 2 in miles
    System.out.println("The total distance was " + totalDistance + " miles"); // Prints total distance of both trips in miles
    System.out.println(); // Creates space in the terminal for output
  }
}