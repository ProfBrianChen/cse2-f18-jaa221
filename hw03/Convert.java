// A program that asks the user for doubles that represent the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average.
// Converts the quantity of rain into cubic miles.

import java.util.Scanner; // Imports the Scanner class for java

public class Convert
{
  public static void main(String[] args) // Main method used in all java programs
  {
    System.out.println(""); // Creates space in the terminal
    Scanner myScanner = new Scanner(System.in); // Creates a scanner object
    System.out.print("Enter the affect area in acres: "); // Askes for user input
    double areaAffected = myScanner.nextDouble(); // Creates variable for user input
    areaAffected *= .0015625; // Converts acres to square miles
    System.out.print("Enter the rainfall in the affected area in inches: "); // Askes for user input
    double rainfall = myScanner.nextDouble(); // Creates variable for user input
    rainfall /= (12 * 5280); // Converts inches to miles
    double rainVolume = areaAffected * rainfall; // Solves for volume in cubic miles
    System.out.println(rainVolume + " cublic miles"); // Displays result
    System.out.println(""); // Creates space in the terminal
  }
}