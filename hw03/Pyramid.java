// A program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid.

import java.util.Scanner; // Imports the Scanner class for java

public class Pyramid
{
  public static void main(String[] args) // Main method used in all java programs
  {
    System.out.println(""); // Creates space in the terminal
    Scanner myScanner = new Scanner(System.in); // Creates scanner object
    System.out.print("The square side of the pyramid is (input length): "); // Askes for user input
    double sideLength = myScanner.nextDouble(); // Creates a variable for user input
    System.out.print("The height of the pyramid is (input height): "); // Askes for user input
    double height = myScanner.nextDouble(); // Creates a variable for user input
    double volume = (Math.pow(sideLength, 2.0) * height) / 3; // Solves for volume
    System.out.println("The volume inside the pyramid is: " + volume + "."); // Displays the volume
    System.out.println(""); // Creates space in the terminal
  }
}