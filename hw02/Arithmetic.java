public class Arithmetic
{
  public static void main(String[] args)
  {
    int numPants = 3;
    double pantsPrice = 34.98;
    
    int numShirts = 2;
    double shirtPrice = 24.99;
    
    int numBelts = 1;
    double beltPrice = 33.99;
    
    double paSalesTax = .06;
    
    double pantsTotal, shirtsTotal, beltsTotal, totalCost; // Creates total variables
    
    pantsTotal = numPants * pantsPrice; // Finds total cost of pants
    shirtsTotal = numShirts * shirtPrice; // Finds total cost of shirts
    beltsTotal = numBelts * beltPrice; // Finds total cost of belts
    
    double pantsTax, shirtsTax, beltsTax, totalTax; // Creates tax variables
    pantsTax = pantsTotal * paSalesTax; // Finds total tax on pants
    shirtsTax = shirtsTotal * paSalesTax; // Finds total tax on shirtss
    beltsTax = beltsTotal * paSalesTax; // Finds total tax on belts
    
    double p = pantsTotal*100.0;
    pantsTotal = (int)p/100.0; // Casts pantsTotal as a double with 2 decimals
    double s = shirtsTotal*100.0;
    shirtsTotal = (int)s/100.0; // Casts shirtssTotal as a double with 2 decimals
    double b = beltsTotal*100.0;
    beltsTotal = (int)b/100.0; // Casts beltsTotal as a double with 2 decimals
    
    p = pantsTax*100.0;
    pantsTax = (int)p/100.0; // Casts pantsTotal as a double with 2 decimals
    s = shirtsTax*100.0;
    shirtsTax = (int)s/100.0; // Casts shirtsTotal as a double with 2 decimals
    b = beltsTax*100.0;
    beltsTax = (int)b/100.0; // Casts beltsTotal as a double with 2 decimals
    
    totalCost = pantsTotal + shirtsTotal + beltsTotal; // Finds total cost of transaction
    totalTax = pantsTax + shirtsTax + beltsTax; // Finds total tax on transaction
    double totalSale = totalCost + totalTax; // Finds total with cost and sales tax
    
    System.out.println(""); // Creates space in terminal
    System.out.println("The pants cost $" + pantsTotal + " with $" + pantsTax + " in sales tax"); // Prints data
    System.out.println("The shirts cost $" + shirtsTotal + " with $" + shirtsTax + " in sales tax"); // Prints data
    System.out.println("The belts cost $" + beltsTotal + " with $" + beltsTax + " in sales tax"); // Prints data
    System.out.println("The total purchase is $" + totalCost); // Prints data
    System.out.println("The total sales tax is $" + totalTax); // Prints data
    System.out.println("The total cost of the purchase with sales tax is $" + totalSale); // Prints data
    System.out.println(""); // Creates space in terminal
   }
}