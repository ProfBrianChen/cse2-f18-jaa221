// ****************************************************************
// Jackson Aguas
// jaa221
// Askes user for a sample String and allows the user to choose an option.
// The user input will determine what is done to the String sampleText.
// ****************************************************************


import java.util.Scanner; // imports Scanner class

public class wordTools{
	public static void main(String[] args){ // Main method used in all java files

		int a = 0;	// Initializes int a
		String choice = ""; // Initializes String choice
		String sampleText = sampleText(); // Runs sampleText method to get an input from user
		while(a < 1){ // Begins a while loop that runs until the program is quit
			switch(choice){ // Begins a switch statement that runs a method based on the user choice
				case "c":
					System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(sampleText)); // Calls method getNumOfNonWSCharacters to get number of non-whitespace characters
					choice = ""; // Resets String choice
					break;
				case "w":
					System.out.println("Number of Words: " + getNumOfWords(sampleText)); // Calls method getNumOfWords to get the number of words
					choice = ""; // Resets String choice
					break;
				case "f":
					Scanner scan = new Scanner(System.in);
					System.out.println("Enter a word or phrase to be found: "); // Prompts user for input
					String word = scan.nextLine(); // Intitalizes String word to store input
					System.out.println(); 
					System.out.println("\"" + word + "\" instances: " + findText(sampleText, word)); // Calls method findText to find the number of instances in a phrase
					choice = ""; // Resets String choice
					break;
				case "r":
					System.out.println();
					System.out.println("Edited text: " + replaceExclamation(sampleText)); // Calls method replaceExclamation to replace "!" with "."
					choice = ""; // Resets String choice
					break;
				case "s":
					System.out.println();
					System.out.println("Edited text: " + shortenSpace(sampleText)); // Calls method shortenSpace to make every instance of multiple spaces a single space
					choice = ""; // Resets String choice
					break;
				case "q":
					a++; // Increments a to exit the while loop
					break;
				default:
					choice = printMenu(); // Calls method printMenu to display the menu and recieve input
					break;
			}	
		}
	}

	public static String sampleText(){ // Method that prompt users to enter a sample text and put it in a string
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a sample text:");
		String s = scan.nextLine();
		System.out.println();
		System.out.print("You entered: " + s);
    System.out.println();
    return s;
	}

	public static String printMenu(){ // Method that displays the menu and records the user input
		System.out.println();
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println();
		System.out.print("Choose an option: ");
		Scanner scan = new Scanner(System.in);
		String choice = scan.nextLine();
		return choice;
		}

	public static int getNumOfNonWSCharacters(String text){ // Method that loops through String sampleText and increments a counter for each non-whitespace char
		int numWhiteSpaces = 0;
		for(int i = 0; i < text.length(); i++){
			int a = 0;
			char b = text.charAt(i);
			if(b == ' '){
				a = 1;
			}
			else if(a < 1){
				numWhiteSpaces++;
			}
			a = 0;
		}
		System.out.println("");
		return numWhiteSpaces;
	}
	

	public static int getNumOfWords(String text){ // Method that loops through the String sampleText and increments a counter for each word found
		int numWords = 0;
		String word = "";
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' '){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' '){
					numWords++;
					word = "";
				}
			}
		}
		System.out.println();
		return (numWords + 1);
	}

	public static int findText(String text, String wordFind){ // Method that loops through the String sampleText and differentiates individual words and tests if they equal the word/phrase that the user inputed
		String word = "";
		int numInstance = 0;
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' ' || text.charAt(i) == '.' || text.charAt(i) == '!' || text.charAt(i) == '?' || text.charAt(i) == ',' || text.charAt(i) == ';'){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' ' || text.charAt(i+1) == '.' || text.charAt(i+1) == '!' || text.charAt(i+1) == '?' || text.charAt(i+1) == ',' || text.charAt(i+1) == ';'){
					if(word.equals(wordFind)){
						numInstance++;
					}
					word = "";
				}
			}
		}

		if((word + text.charAt(text.length()-1)).equals(wordFind)){
			numInstance++;
		}
		return numInstance;
	}

	public static String replaceExclamation(String text){ // Method that takes in the String sampleText and replaces each instance of a "!" with a "."
		String editedText = text.replace('!','.');
		System.out.println();
		return editedText;
	}

	public static String shortenSpace(String text){ // Method that loops through the String sampleText and differentiates each word and removes the surrounding spaces and then adds it string with only a single space
		String word = "";
		String editedText = "";
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' '){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' '){
					editedText += (word + " ");
					word = "";
				}
			}
		}
		editedText += (word + text.charAt(text.length()-1));
		return editedText;	
	}
}


