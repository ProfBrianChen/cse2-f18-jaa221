//*****************************************************************************************
// Poker
// This program uses while loops to calculate the probablity of certain hands in poker
//*****************************************************************************************

import java.util.Scanner; // imports Scanner class
import java.util.Random; // imports Random class

public class poker
{
  public static void main(String[] args) // method used to start all java programs
  {
    Scanner scan = new Scanner(System.in); // creates a scanner object
    boolean correct = false; // intializes boolean to check if correct
    int numHands = 0; // initializes variable numHands
    int l = 0; // intitializes variable l
    int m = 0; // initializes variable m
    int n = 0; // initializes variable n
    int i = 0; // initializes variable i
    int ace = 0; // initializes variable ace
    int two = 0; // initializes variable two
    int three = 0; // initializes variable three
    int four = 0; // initializes variable four
    int five = 0; // initializes variable five 
    int six = 0; // initializes variable six
    int seven = 0; // initializes variable seven
    int eight = 0; // initializes variable eight
    int nine = 0; // initializes variable nine
    int ten = 0; // initializes variable ten
    int jack = 0; // initializes variable jack
    int queen = 0; // initializes variable queen
    int king = 0; // initializes variable king
    int[] hand = {0,0,0,0,0}; // initializes an array to store the values of the hand
    int pair = 0; // initializes variable pair
    int triple = 0; // initializes variable triple
    int quad = 0; // initializes variable quad
    int onePair = 0; // initializes variable onePair
    int twoPair = 0; // initializes variable twoPair
    int threeOfKind = 0; // initializes variable threeOfKind
    int fourOfKind = 0; // initializes variable fourOfKind
    
    while(l<1){
      System.out.print("How many hands would you like generated: "); // prompts user
      correct = scan.hasNextInt(); // checks if input is an integer
      if(correct){
        numHands = scan.nextInt(); // creates variable that stores user input
        correct = false; // resets boolean
        l++; // increments variable
       }
      else{
        scan.next(); // clears scanner object
        System.out.println("Please enter an integer"); 
      }
    }
    
    
    while(n < numHands){ // begins a while loop that runs for every hand generated
      while(i < 5) { // begins a while loop that builds a hand of cards
        hand[i] = (int)(Math.random() * 52) + 1; // creates a random number to represent a card
        
        switch (hand[i]%13){ // begins a switch statement to count the occurences of each card type in a hand
          case 0:
            ++ace; // increments variable
            break;
          case 1:
            ++two; // increments variable
            break;
          case 2:
            ++three; // increments variable
            break;
          case 3:
            ++four; // increments variable
            break;
          case 4:
            ++five; // increments variable
            break;
          case 5:
            ++six; // increments variable
            break;
          case 6:
            ++seven; // increments variable
            break;
          case 7:
            ++eight; // increments variable
            break;
          case 8:
            ++nine; // increments variable
            break;
          case 9:
            ++ten; // increments variable
            break;
          case 10:
            ++jack; // increments variable
            break;
          case 11:
            ++queen; // increments variable
            break;
          case 12:
            ++king; // increments variable
            break;
        }
        ++i; // increments variable
      }
      i = 0; // rests variable

      int[] type = {ace,two,three,four,five,six,seven,eight,nine,ten,jack,queen,king}; // intializes an array that stores the number of card types of the hand

      while(m < 13){  // begins a while loop that checks for matching card types              
        if (type[m] == 2){ // checks for a pair
          ++pair; // increments variable
        }
        else if (type[m] == 3){ // checks for a triple
          ++triple; // increments variable
        }
        else if(type[m] == 4){ // checks for a quad
          ++quad; // increments variable
        }
        ++m; // increments variable
      }
      m = 0; // resets variable

      if(pair == 1 && triple != 1){ // checks for one pair
        ++onePair; // increments variable
      } 
      else if(pair == 2){ // checks for two pair
        ++twoPair; // increments variable
      }
      else if(triple == 1 && pair != 1){ // checks for three of a kind
        ++threeOfKind; // increments variable
      }
      else if(quad == 1){ // checks for four of a kind
        ++fourOfKind; // increments variable
      }

      ace = 0; // resets variable
      two = 0; // resets variable
      three = 0; // resets variable
      four = 0; // resets variable
      five = 0; // resets variable
      six = 0; // resets variable
      seven = 0; // resets variable
      eight = 0; // resets variable
      nine = 0; // resets variable
      ten = 0; // resets variable
      jack = 0; // resets variable
      queen = 0; // resets variable
      king = 0; // resets variable
      pair = 0; // resets variable
      triple = 0; // resets variable
      quad = 0; // resets variable
      ++n; // increments variable 
    }

    double probPair = ((int)((onePair*1000)/numHands))/1000.0; // initializes variable and calculates probability to three decimals 
    double probTwoPair = ((int)((twoPair*1000)/numHands))/1000.0; // initializes variable and calculates probability to three decimals 
    double probThreeOfKind = ((int)((threeOfKind*1000)/numHands))/1000.0; // initializes variable and calculates probability to three decimals 
    double probFourOfKind = ((int)((fourOfKind*1000)/numHands))/1000.0; // initializes variable and calculates probability to three decimals 

    System.out.println(); // creates space in terminal
    System.out.println("The number of loops: " + numHands); // displays number of loops
    System.out.println("The probability of One-pair: " + probPair); // displays probability of one pair
    System.out.println("The probability of Two-pair: " + probTwoPair); // displays probability of two pair
    System.out.println("The probability of Three-of-a-kind: " + probThreeOfKind); // displays probability of three of a kind
    System.out.println("The probability of Four-of-a-kind: " + probFourOfKind); // displays probability of four of a kind
    System.out.println(); // creates space in terminal
  }
}