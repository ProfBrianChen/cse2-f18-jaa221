import java.util.Scanner; // Imports Scanner class
import java.util.Random; // Imports Random class

public class CrapsSwitch
{
  public static void main(String[] args) // Main Java method
  {
    Scanner myScanner = new Scanner(System.in); // Creates a scanner object
    System.out.print("Would you like your dice random (0) or picked (1), choose enter a number: "); // Requests user input
    int choice = myScanner.nextInt(); // Creates and stores a variable for user choice
    int dice1 = 0; // Intitalizes dice1 
    int dice2 = 0; // Intitalizes dice2
    String slang = ""; // Intitalizes slang
    
    switch (choice) { // Switch statement that reads the user choice and fills the dice variables with the chosen method
      case 0:
        dice1 = (int)(Math.random() * 5) + 1; // Creates random integer from 1-6 and store it in dice1
        dice2 = (int)(Math.random() * 5) + 1; // Creates random integer from 1-6 and store it in dice2
        break;
      case 1: 
        System.out.print("Enter first roll (1-6): "); // Asks for user input
        dice1 = myScanner.nextInt(); // Stores value in dice1
        System.out.print("Enter second roll (1-6): "); // Asks for user input
        dice2 = myScanner.nextInt(); // Stores value in dice2
        break;
      default:
        System.out.println("Please enter a valid input."); // Error handling
    }
    
    switch (dice1 + dice2) { // Switch statement that determines what the sum of the two dice is
      case 2:
        slang = "Snake Eyes"; // Assigns slang term
        break;
      case 3:
        slang = "Ace Deuce"; // Assigns slang term
        break;
      case 4:
        switch (dice1) { // Switch statements that determines if hand is "hard" or "easy"
          case 2:
            slang = "Hard Four"; // Assigns slang term
            break;
          default:
            slang = "Easy Four"; // Assigns slang term
        }
        break;
      case 5:
        slang = "Fever Five"; // Assigns slang term
        break;
      case 6:
        switch (dice1) { // Switch statements that determines if hand is "hard" or "easy"
          case 3:
            slang = "Hard Six"; // Assigns slang term
            break;
          default:
            slang = "Easy Six"; // Assigns slang term
        }
        break;
      case 7:
        slang = "Seven Out"; // Assigns slang term
        break;
      case 8:
        switch (dice1) { // Switch statements that determines if hand is "hard" or "easy"
          case 4:
            slang = "Hard Eight"; // Assigns slang term
            break;
          default:
            slang = "Easy Eight"; // Assigns slang term
        }
        break;
      case 9:
        slang = "Nine"; // Assigns slang term
        break;
      case 10:
        switch (dice1) { // Switch statements that determines if hand is "hard" or "easy"
          case 5:
            slang = "Hard Ten"; // Assigns slang term
            break;
          default:
            slang = "Easy Ten"; // Assigns slang term
        }
        break;
      case 11:
        slang = "Yo-leven"; // Assigns slang term
        break;
      case 12:
        slang = "Boxcars"; // Assigns slang term
        break;
    }
    System.out.println(""); // Creates space in terminal
    System.out.println("You have a " + slang); // Displays the slang term
    System.out.println(""); // Creates space in terminal
  }
}