import java.util.Scanner; // Imports Scanner class
import java.util.Random; // Imports Random class

public class CrapsIf
{
  public static void main(String[] args) // Main Java method
  {
    Scanner myScanner = new Scanner(System.in); // Creates a scanner object
    System.out.print("Would you like your dice random (0) or picked (1), choose enter a number: "); // Requests user input
    int choice = myScanner.nextInt(); // Creates and stores a variable for user choice
    int dice1 = 0; // Intitalizes dice1
    int dice2 = 0; // Intitalizes dice2
    String slang = ""; // Intitalizes slang
    if (choice == 0) {
      dice1 = (int)(Math.random() * 5) + 1; // Creates random integer from 1-6 and store it in dice1
      dice2 = (int)(Math.random() * 5) + 1; // Creates random integer from 1-6 and store it in dice2
    }
    else if (choice == 1) {
      System.out.print("Enter first roll (1-6): "); // Asks for user input
      dice1 = myScanner.nextInt(); // Stores value in dice1
      System.out.print("Enter second roll (1-6): "); // Asks for user input
      dice2 = myScanner.nextInt(); // Stores value in dice2
    }
    else{
      System.out.println("Please enter a valid input."); // Error handling
    }

    if (dice1 == 1 || dice2 == 1) { // Checks all combinations of dice when at least one is 1 and assigns a slang term
        if(dice1 == 1 && dice2 == 1) {
          slang = "Snake Eyes"; // Assigns slang term
        }
        else if (dice1 == 2 || dice2 == 2) {
          slang = "Ace Deuce"; // Assigns slang term
        }
        else if (dice1 == 3 || dice2 == 3) {
          slang = "Easy Four"; // Assigns slang term
        }
        else if (dice1 == 4 || dice2 == 4) {
          slang = "Fever Five"; // Assigns slang term
        }
        else if (dice1 == 5 || dice2 == 5) {
          slang = "Easy Six"; // Assigns slang term
        }
        else if (dice1 == 6 || dice2 == 6) {
          slang = "Seven Out"; // Assigns slang term
        }
      }
      if (dice1 == 2 || dice2 == 2) { // Checks all combinations of dice when at least one is 2 and assigns a slang term
        if(dice1 == 2 && dice2 == 2) {
          slang = "Hard Four"; // Assigns slang term
        }
        else if (dice1 == 3 || dice2 == 3) {
          slang = "Fever Five"; // Assigns slang term
        }
        else if (dice1 == 4 || dice2 == 4) {
          slang = "Easy Six"; // Assigns slang term
        }
        else if (dice1 == 5 || dice2 == 5) {
          slang = "Seven Out"; // Assigns slang term
        }
        else if (dice1 == 6 || dice2 == 6) {
          slang = "Easy Eight"; // Assigns slang term
        }
      }
      if (dice1 == 3 || dice2 == 3) { // Checks all combinations of dice when at least one is 3 and assigns a slang term
        if(dice1 == 3 && dice2 == 3) {
          slang = "Hard Six"; // Assigns slang term
        }
        else if (dice1 == 4 || dice2 == 4) {
          slang = "Seven Out"; // Assigns slang term
        }
        else if (dice1 == 5 || dice2 == 5) {
          slang = "Easy Eight"; // Assigns slang term
        }
        else if (dice1 == 6 || dice2 == 6) {
          slang = "Nine"; // Assigns slang term
        }
      }
      if (dice1 == 4 || dice2 == 4) { // Checks all combinations of dice when at least one is 4 and assigns a slang term
        if(dice1 == 4 && dice2 == 4) {
          slang = "Hard Eight"; // Assigns slang term
        }
        else if (dice1 == 5 || dice2 == 5) {
          slang = "Nine"; // Assigns slang term
        }
        else if (dice1 == 6 || dice2 == 6) {
          slang = "Easy Ten"; // Assigns slang term
        }
      }
      if (dice1 == 5 || dice2 == 5) { // Checks all combinations of dice when at least one is 5 and assigns a slang term
        if(dice1 == 5 && dice2 == 5) {
          slang = "Hard Ten"; // Assigns slang term
        }
        else if (dice1 == 6 || dice2 == 6) { 
          slang = "Yo-leven"; // Assigns slang term
        }
      }
      if (dice1 == 6 && dice2 == 6) { // Checks if both dice are 6 and assigns proper slang
        slang = "Boxcars"; // Assigns slang term
      }
    System.out.println(""); // Creates space in terminal
    System.out.println("You have a " + slang); // Displays the slang term
    System.out.println(""); // Creates space in terminal
  }
}