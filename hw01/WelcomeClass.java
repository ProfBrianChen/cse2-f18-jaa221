public class WelcomeClass
{
  public static void main(String[] args)
  {
     System.out.println("    ----------- ");
     System.out.println("    | WELCOME |" );
     System.out.println("  ^  ^  ^  ^  ^  ^  ");
     System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
     System.out.println("<-J--A--A--2--2--1-> ");
     System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
     System.out.println("  v  v  v  v  v  v ");
     System.out.println(" Hi, I am Jackson Aguas.\n I was born in San Fransico, CA on May 25, 1999.\n I am currently studying mechanical engineering at Lehigh University\n where I am a sophomore.");
  }
}