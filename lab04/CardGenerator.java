import java.util.Random; // Imports the Random class

public class CardGenerator
{
  public static void main(String[] args) // Main method used in all java program
  {
    int randNum = (int)(Math.random() * 51); // Randomly generates a number from 0-51 
//     System.out.println(randNum); // Used to test program
//     System.out.println(randNum % 13); // Used to test program
    String suit = ""; // Intializes a variable for suit
    String num = ""; // Intializes a variable for identity
    if (randNum < 13 && randNum >= 0) { 
      suit = "Diamonds"; // Assigns the diamond suit if the number fit the criteria
    }
    else if (randNum < 26 && randNum >= 13) {
      suit = "Clubs"; // Assigns the club suit if the number fit the criteria
    }
    else if (randNum < 39 && randNum >= 26) {
      suit = "Hearts"; // Assigns the hearts suit if the number fit the criteria
    }
    else {
      suit = "Spades"; // Assigns the club suit if the number fit the criteria
    }
   
    switch (randNum % 13) { // A switch statement that assigns the identity based on a number 0-12 within each suit (uses modulus)
      case 0:
        num = "Ace"; // Assigns identity
        break;
      case 1:
        num = "2"; // Assigns identity
        break;
      case 2:
        num = "3"; // Assigns identity
        break;
      case 3:
        num = "4"; // Assigns identity
        break;
      case 4:
        num = "5"; // Assigns identity
        break;
      case 5:
        num = "6"; // Assigns identity
        break;
      case 6:
        num = "7"; // Assigns identity
        break;
      case 7:
        num = "8"; // Assigns identity
        break;
      case 8:
        num = "9"; // Assigns identity
        break;
      case 9:
        num = "10"; // Assigns identity
        break;
      case 10:
        num = "Jack"; // Assigns identity
        break;
      case 11:
        num = "Queen"; // Assigns identity
        break;
      case 12:
        num = "King"; // Assigns identity
        break;
    }
    System.out.println(""); // Creates space in the terminal 
    System.out.println("You picked the " + num + " of " + suit); // Displays the selected card
    System.out.println(""); // Creates space in the terminal
  }
}