// ****************************************************************
// Jackson Aguas
// jaa221
// Print out all the cards in a deck, shuffles the whole deck of cards, then prints out the cards in the deck, all shuffled, 
// then gets a hand of cards and prints them out too, then it continues to ask the user to draw another hand and draws a new hand if selected.
// ****************************************************************

import java.util.Scanner; // imports Scanner 
import java.util.Random; // imports Random

public class Shuffling
{ 
	public static void main(String[] args)  // mainmethod used in all java programs
	{ 
		Scanner scan = new Scanner(System.in); 
		 //suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  // System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = index - numCards;
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt();
		   if(numCards > index){ 	// an if statement that checks if numCards > index
		   		index = 51;			// rests index to 51
		   		shuffle(cards); 	// calls method shuffle for String[] cards
		   		printArray(cards);	// call method printArray for String[] cards
		   } 
		}  
	}

	public static void shuffle(String[] deck){ // a method that take in a list of Strings and swaps a random String in the array with String[0] many times in order to randomize
		int m = 0;
		String n = "";
		for(int i = 0; i < 1000; i++){
			m = (int)(Math.random()*52);
			n = deck[m];
			deck[m] = deck [0];
			deck[0] = n;
			n = "";
		}
	} 

	public static String[] getHand(String[] deck, int index, int numCards){ // a method that takes in a list of Strings, an int index, and an int numCards in order to draw the Strings in a position designated by index, numCard times and creates another array of the Strings 
		String[] hand = new String[numCards];
		int c = index;
		if(numCards > c){
			shuffle(deck);
			System.out.println(c);
			c = 51;
		}
		for(int i = 0; i < numCards; i++){
				hand[i] = deck[c];
				c--;
			}
		return hand;
	}

	public static void printArray(String[] deck){ // a method that takes in a list of Strings and diplays them in the terminal with spaces inbetween the members
		System.out.println();
		for(int i = 0; i < deck.length; i++){
			System.out.print(deck[i] + " ");
		}
		System.out.println();
	}

}

