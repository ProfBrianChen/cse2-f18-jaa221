public class methods
{
  public static void main(String args[])
  {      
    int[] array0 = new int[8];
    for(int i = 0; i < 8; i++){
      array0[i] = i;
    }
    int[] array1 = new int[8];
    int[] array2 = new int[8];
    int[] array3 = new int[8];
    array1 = copy(array0);
    array2 = copy(array0);
    inverter(array0);
    inverter2(array1);
    array3 = inverter2(array2);
    print(array0);
    print(array1);
    print(array3);
  }

  public static int[] copy(int[] arr)
  {
    int[] arrCopy = new int[8];
    for(int i = 0; i < 8; i++){
      arrCopy[i] = arr[i];
    }
    return arrCopy;
  }

  public static void inverter(int[] arr)
  {
    for(int i = 0; i < 8; i++){
      arr[7 - i] = i;
    }
  }

  public static int[] inverter2(int[] arr)
  {
    int[] arrInvert = new int[8];
    int[] arrCopy = new int[8];
    for(int i = 0; i < 8; i++){
      arrInvert[7 - i] = arr[i];
    }
    return arrInvert;
  }

  public static void print(int[] arr)
  {
    System.out.println();
    for(int i = 0; i < 8; i++){
      System.out.print(arr[i] + " ");
    }
    System.out.println();
  }
}
