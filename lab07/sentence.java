import java.util.Random;
import java.util.Scanner;

public class sentence
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    System.out.print("How many sentences would you like generated: ");
    int m = scan.nextInt();
    for(int i = 0; i < m; i++){
      String subj = nonPrimaryNounsSub(); 
      String thesis = "The " + adjective() + " " + adjective() + " " + subj + " " + pastTenseVerb() + " the " + adjective() + " " +  nonPrimaryNounsObj() + ".";
      System.out.println(thesis);
      System.out.println(support(subj));
      thesis = "";
    }
    
    
  }
  
  public static String support(String subj){
    String s = "This " + subj + " was " + adjective() + "ly " + adjective() + " to the " + adjective() + " " + nonPrimaryNounsObj() + ". It used " + nonPrimaryNounsObj() + "s to " + pastTenseVerb() + " " + nonPrimaryNounsObj() + " at the " + adjective() + " " + nonPrimaryNounsSub() + ".";
    return s;
  }
  public static String adjective(){
    Random rand = new Random();
    int n = rand.nextInt(10);
    String s = "";
    switch(n){
      case 0:
        s = "harsh";
        break;
      case 1:
        s = "supreme";
        break;
      case 2:
        s = "tremendous";
        break;
      case 3:
        s = "simple";
        break;
      case 4:
        s = "unbecoming";
        break;
      case 5:
        s = "probable";
        break;
      case 6:
        s = "wasteful";
        break;
      case 7:
        s = "wacky";
        break;
      case 8:
        s = "devilish";
        break;
      case 9:
        s = "immense";
        break;
    }
    return s;
  }
  public static String nonPrimaryNounsSub(){
    Random rand = new Random();
    int n = rand.nextInt(10);
    String s = "";
    switch(n){
      case 0:
        s = "floor";
        break;
      case 1:
        s = "train";
        break;
      case 2:
        s = "giant";
        break;
      case 3:
        s = "representative";
        break;
      case 4:
        s = "watch";
        break;
      case 5:
        s = "wall";
        break;
      case 6:
        s = "kettle";
        break;
      case 7:
        s = "ticket";
        break;
      case 8:
        s = "person";
        break;
      case 9:
        s = "ground";
        break;
    }
    return s;
  }
  public static String pastTenseVerb(){
    Random rand = new Random();
    int n = rand.nextInt(10);
    String s = "";
    switch(n){
      case 0:
        s = "heated";
        break;
      case 1:
        s = "interested";
        break;
      case 2:
        s = "whispered";
        break;
      case 3:
        s = "offered";
        break;
      case 4:
        s = "tumbled";
        break;
      case 5:
        s = "hoped";
        break;
      case 6:
        s = "subtracted";
        break;
      case 7:
        s = "wacked";
        break;
      case 8:
        s = "pulled";
        break;
      case 9:
        s = "tried";
        break;
    }
    return s;
  }
  public static String nonPrimaryNounsObj(){
    Random rand = new Random();
    int n = rand.nextInt(10);
    String s = "";
    switch(n){
      case 0:
        s = "dog";
        break;
      case 1:
        s = "baby";
        break;
      case 2:
        s = "egg";
        break;
      case 3:
        s = "nose";
        break;
      case 4:
        s = "cat";
        break;
      case 5:
        s = "friend";
        break;
      case 6:
        s = "bird";
        break;
      case 7:
        s = "rail";
        break;
      case 8:
        s = "leg";
        break;
      case 9:
        s = "rock";
        break;
    }
    return s;
  }
}