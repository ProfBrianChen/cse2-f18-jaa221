// ****************************************************************
// Jackson Aguas
// jaa221
// Askes the user for an integer between 0 - 100. Validates input.
// Creates a pattern of *s that forms an X shape with the dimensions of the input.
// ****************************************************************

import java.util.Scanner; // imports Scanner class

public class EncryptedX
{
	public static void main(String[] args) // method used for all java program
	{
		int a = 0; // initializes int variable a
		int b = 0; // initializes int variable b 
		int n = 0; // initializes int variable n
		String x = ""; // initializes string variable x
		boolean correct = false;  // initializes boolean variable correct;
		Scanner scan = new Scanner(System.in);  // builds scanner clas
		System.out.print("Enter an integer between 0 and 100: "); // asks user for input
	    while(a < 1){ // begins a while loop that checks the correctness of the user input
		    correct = scan.hasNextInt(); // checks if an int in scanner
        	if(correct){ // checks for the range of the int inputed
            	b = scan.nextInt(); // inserts scanner value into a variable
            	if(correct && b > 0 && b < 100){
		            n = b; // transfers the value of b to a more global variable n
		            correct = false; // resets boolean correct
		            b = 0; // resets int b;
		            a++; // increments variable a
		        }
		        else{
		            System.out.print("Enter an integer between 0 and 100: "); // asks for user input
		        }
		    }
		    else{
		        scan.next(); // clears scanner
		        System.out.print("Enter an integer between 0 and 100: "); // asks for user input
		    }
		}
		for(int i = 0; i <= n; i++){ // begins a for loop that builds the X pattern
			for(int j = 0; j <= n; j++){ // begins a for loops that builds the individual lines of the pattern
				if(j == i){ // checks for correct space for a " " and inserts one
					x += " "; // inserts a " " into string x
				}
				else if(j == (n - i)){ // checks for correct space for a " " and inserts one
					x += " "; // inserts a " " into string x

				}
				else{ // checks for correct space for a "*" and inserts one
					x += "*"; // inserts a "*" into string x
				}
			}
			System.out.println(x); // prints out the individual lines of the X pattern
			x = ""; // resets the string x
		}
	}
}