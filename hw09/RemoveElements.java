// ****************************************************************
// Jackson Aguas
// jaa221
// Uses three methods to build an array of random ints, remove a member by index,
// and remove elements by values.
// ****************************************************************

import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
  	Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
  	String answer="";
  	do{
    	System.out.println("Random input 10 ints [0-9]");
    	num = randomInput();
    	String out = "The original array is:";
    	out += listArray(num);
    	System.out.println(out);
   
    	System.out.print("Enter the index ");
    	index = scan.nextInt();
    	newArray1 = delete(num,index);
    	String out1="The output array is ";
    	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out1);
   
      System.out.print("Enter the target value ");
    	target = scan.nextInt();
    	newArray2 = remove(num,target);
    	String out2="The output array is ";
    	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out2);
    	 
    	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    	answer=scan.next();
  	}while(answer.equals("Y") || answer.equals("y"));
    }
   
  public static String listArray(int num[]){
  	String out="{";
  	for(int j=0;j<num.length;j++){
    	if(j>0){
      	out+=", ";
    	}
    	out+=num[j];
  	}
  	out+="} ";
  	return out;
  }

  public static int[] randomInput(){ // Method that builds an int[] of randomly generated values from 0-9
    int[] list = new int[10];
    for(int i = 0; i < 10; i++){
      list[i] = (int)(Math.random()*9);
    }
    return list;
  }

  public static int[] delete(int[] list, int pos){ // Method that takes in and loops through an int[] and builds a new int[] with the removed index chosen by the user
    int[] newList = new int[list.length-1];
    int n = 0;
    int c = 0;
    if(pos >= 0 && pos < list.length){
      for(int i = 0; i < list.length; i++){
        n = list[i];
        if(i != pos){
          newList[c] = list[i];
          c++;
        }
      }
      System.out.println("Index " + pos + " element is removed");
      return newList;
    }
    else if(pos < 0 || pos > list.length){
      System.out.println("The index is not valid");
    }
    return list;
  }

  public static int[] remove(int[] list, int target){ // Method that takes in and loops through an int[] and builds a new int[] with the removed elements with a value chosen by the user
    int c = 0;
    for(int i = 0; i < list.length; i++){
      if(list[i] == target){
        c++;
      }
    }
    if(c != 0){
      int[] newList = new int[list.length-c];
      int k = 0; 
      for(int j = 0; j < list.length; j++){
        if(list[j] != target){
          newList[k] = list[j];
          k++;
        }
      }
      System.out.println("Element " + target + " has been removed");
      return newList;
    }
    else{
      System.out.println("Element " + target + " was not found");
      return list;
    }
  }
}