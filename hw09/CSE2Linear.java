// ****************************************************************
// Jackson Aguas
// jaa221
// Asks user to input 15 ascending ints and gives messages depending on whats entered. 
// Uses a method to perform a binary search for an element chosen by the user.
// Then uses a method to scramble the members of the array and uses another method that 
// performs a linear search for another element chosen by the user.
// ****************************************************************

import java.util.Scanner; // Imports Scanner class
import java.util.Random; // Imports Random class

public class CSE2Linear
{
	public static void main(String[] args) // Main method used in all java programs
	{
		Scanner scan = new Scanner(System.in);
		int[] grades = new int[15];
		System.out.println("Enter 15 ascending ints for final grades in CSE2:");
		int m = 0;
		int n = 0;
		int a = 0;
		int i = 0;
		boolean isGood = false;

		while(i < 15){
			if(scan.hasNextInt()){
				n = scan.nextInt();
				isGood = true;
				if(n < 0 || n > 100){
					System.out.println("Input is out of range");
					n = 0;
					isGood = false;
				}
				else if(n < m){
					System.out.println("Input is not greater than or equal to the last int");
					n = 0;
					isGood = false;
				}
			}
			else if(!scan.hasNextInt()){
				System.out.println("Input is not an int");
				scan.next();
				n = 0;
				isGood = false;
			}
			if(isGood){
				m = n;
				grades[i] = n;
				n = 0;
				isGood = false;
				i++;
			}
		}
		for(int c = 0; c < grades.length; c++){
			System.out.print(grades[c] + " ");
		}
		System.out.println();
		binarySearch(grades);
		scramble(grades);
		System.out.println("Scrambled:");
		for(int c = 0; c < grades.length; c++){
			System.out.print(grades[c] + " ");
		}
		System.out.println();
		linearSearch(grades);
	}

	public static void binarySearch(int[] grades){ // Method that takes in an int[] and performs a binary search that coninues to split the array in half until a value is found
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter a grade to search for: ");
		int find = scan.nextInt();
		int iterations = 0;
		Boolean found = false;
		if(!found){
			if(find == grades[7]){
				iterations = 1;
				found = true;
			}
			else if(find < grades[7]){
				if(find == grades[3]){
						iterations = 2;
						found = true;
				}
				else if(find < grades[3]){
					if(find == grades[1]){
						iterations = 3;
						found = true;
					}
					else if(find < grades[1]){
						if(find == grades[0]){
							iterations = 4;
							found = true;
						}
					}
					else if(find > grades[1]){
						if(find == grades[2]){
							iterations = 4;
							found = true;
						}
					}
				}
				else if(find > grades[3]){
					if(find == grades[5]){
						iterations = 3;
						found = true;
					}
					else if(find < grades[5]){
						if(find == grades[4]){
							iterations = 4;
							found = true;
						}
					}
					else if(find > grades[5]){
						if(find == grades[6]){
							iterations = 4;
							found = true;
						}
					}
				}
			}
			else if(find > grades[7]){
				if(find == grades[11]){
					iterations = 2;
				}
				else if(find < grades[11]){
					if(find == grades[9]){
						iterations = 3;
						found = true;
					}
					else if(find < grades[9]){
						if(find == grades[8]){
							iterations = 4;
							found = true;
						}
					}
					else if(find > grades[9]){
						if(find == grades[10]){
							iterations = 4;
							found = true;
						}
					}
				}
				else if(find > grades[11]){
					if(find == grades[13]){
						iterations = 3;
						found = true;
					}
					else if(find < grades[13]){
						if(find == grades[12]){
							iterations = 4;
							found = true;
						}
					}
					else if(find > grades[13]){
						if(find == grades[14]){
							iterations = 4;
							found = true;
						}
					}
				}
			}
		}
		if(found){
			System.out.println(find + " was found in the list with " + iterations + " iterations");
		}
		else{
			iterations = 4;
			System.out.println(find + " was not found in the list with " + iterations + " iterations");
		}
	}

	public static void scramble(int[] grades){ // Method that takes in an int[] and scrambles the elements by coninuously random switching members with the first members
		int a = 0;
		int b = 0;
		for(int i = 0; i < 1000; i++){
			a = (int)(Math.random()*15);
			b = grades[a];
			grades[a] = grades[0];
			grades[0] = b;
			b = 0;
		}
	}

	public static void linearSearch(int[] grades){ // Method that takes in an int[] and performs a liner search by looping through the array until a matching value is found
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter a grade to search for: ");
		int find = scan.nextInt();
		int iterations = 0;
		Boolean found = false;
		for(int i = 0; i < 15; i++){
			if(grades[i] == find && !found){
				iterations = i + 1;
				found = true;
			}
		}
		if(found){
			System.out.println(find + " was found in the list with " + iterations + " iterations");
		}
		else{
      iterations = 15;
			System.out.println(find + " was not found in the list with " + iterations + " iterations");
		}
	}
}