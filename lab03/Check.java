//uses the Scanner class to obtain from the user 
//the original cost of the check, 
//the percentage tip they wish to pay, 
//and the number of ways the check will be split. 
//Then determine how much each person in the group needs to 
//spend in order to pay the check.

import java.util.Scanner; // Imports the Scanner class for java
public class Check
{
  public static void main(String[] args) // Main method used in all java programs
  {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); // Gets check cost as input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form x) : ");
    double tipPercent = myScanner.nextDouble(); // Gets tip percentage as an input
    tipPercent /= 100; // Converting percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); // Gets the number of prople as an input
    
    double totalCost, costPerPerson; // Creates variables for total cost and the cost per person 
    int dollars; // Whole dollar amount of cost 
    int dimes, pennies; // For storing digits to the right of the decimal point for the cost
    totalCost = checkCost * (1 + tipPercent); // Gets the cost plus tip
    costPerPerson = totalCost / numPeople; // Gets the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson; // Gets dimes amount
    dimes = (int)(costPerPerson * 10) % 10; // Gets the dime amount
    pennies = (int)(costPerPerson * 100)% 10; // Gets the penny amount 
    System.out.println(); // Creates space in the terminal
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // Prints out the amount each person has to pay
    System.out.println(); // Creates space in the terminal
    
  }
}